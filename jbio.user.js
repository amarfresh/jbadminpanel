// ==UserScript==
// @name        GoInhouse.com popup admin
// @namespace   https://www.goinhouse.com
// @version      0.3
// @description  
// @author      amarfresh
// @match       *://*.goinhouse.com/*
// @match       *://*.dataprivacycareers.com/*
// @match       *://*.gobiglaw.com/*
// @match       *://*.jobboard.io/*
// @run-at      document-end
// @grant     none
// ==/UserScript==

// READ: EDIT THE @namespace and @match values above with your own jobboard.io domain name.

var currentUrlPath  = window.location.pathname;
/*--- Test that "/jobs" is in URL
*/
if  (/\/jobs/.test (currentUrlPath) && (!(/\/admin/.test (currentUrlPath) ) )) {

    var newURL  = window.location.protocol + "//"
                + window.location.host
                + "/admin" + currentUrlPath + "/edit/"
                + window.location.search
                + window.location.hash
                ;
    /*-- replace() puts the good page in the history instead of the
        bad page.
    */
//    window.location.replace (newURL);

var el = document.createElement('div');
el.setAttribute('style', 'color:#000;font-weight:bold;font-size:10px;line-height:40px;background:rgba(200,200,200,0.8);border:1px solid #555;position:fixed;top:20px;right:20px;width:100px;height:40px;z-index:99999;');
el.innerHTML = "<a href=\"" + newURL + "\">Edit<\/a>";
document.body.appendChild(el);

}
if  (/\/jobs/.test (currentUrlPath) && (/\/admin/.test (currentUrlPath)  )) {

 var checkURL = ['ind', 'IND', 'indeed', 'INDEED', 'Indeed', 'INDD', 'indd', 'Indd' ];
 var daURL   = jQuery('#job_apply_url').val();
 var CANADIAN = false;
    
    for (var i = 0; i < checkURL.length; i++) {

        if(daURL.indexOf(checkURL[i]) > -1) {
        CANADIAN = true;
        }
    }
var yourRandomGenerator=function(rangeOfDays,startHour,hourRange){
    var today = new Date(Date.now());
    return new Date(today.getYear()+1900,today.getMonth(), today.getDate()+1+Math.random() *rangeOfDays, Math.random()*hourRange + startHour, Math.random()*60);
}

newDate=yourRandomGenerator(0,1,5);
newDate=newDate.toString();
newDate=newDate.substring(0,(newDate.length-11));
    
if (CANADIAN == true) {
jQuery('#job_apply_url').css('background', 'red');
var el = document.createElement('div');
el.setAttribute('style', '.color:#000;font-weight:bold;font-size: 12px;line-height:40px;background:rgba(200,200,200,0.8);border:1px solid #555;padding: 10px;width: 800px;margin: auto;position: absolute;top: 1650px;right: 150px;left: 100px; z-index:99999;');
el.innerHTML = "<textarea rows='2' cols='110'>"+ daURL + "</textarea>";
document.body.appendChild(el);
}
jQuery('#job_apply_url').after('<a href="'+jQuery('#job_apply_url').val()+'" target="_new"><span class="glyphicon glyphicon-share"></span></a>');
jQuery('.actions').clone().appendTo('.form-group:first');
jQuery('#created_at').after('<a href="#/" onclick="jQuery(\'#job_created_at\').val(newDate);"><span class="glyphicon glyphicon-time"></span></a>');
console.log(yourRandomGenerator(0,0,4));

    

}